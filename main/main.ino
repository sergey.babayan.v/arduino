#include <Wire.h>
#include <BH1750.h>         
BH1750 lightMeter;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  lightMeter.begin();
  Serial.println("Running...");
}

void loop() {
  // put your main code here, to run repeatedly:
  uint16_t lux = lightMeter.readLightLevel();
  Serial.print(lux);
  delay(500);
}
