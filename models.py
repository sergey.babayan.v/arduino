from pinform import Measurement
from pinform.fields import FloatField


class Record(Measurement):
    value = FloatField(null=False)

    class Meta:
        measurement_name = "light"
