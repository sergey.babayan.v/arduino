import os, sys
import socket
from time import sleep

import serial

ARDUINO_ADDR = sys.argv[1]

arduino_com = serial.Serial(ARDUINO_ADDR, 9600)
sleep(2)
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(("localhost", 8080))
server.listen(5)
conn, addr = server.accept()
while True:
    line = arduino_com.readline()
    conn.send(line)
