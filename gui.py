# Графический интерфейс
import json
import tkinter as tk
import threading
import socket
import webbrowser
from datetime import datetime
from tkinter.scrolledtext import ScrolledText

from influxdb import InfluxDBClient


class CommunicateThread(threading.Thread):
    def __init__(self, app, format):
        super().__init__()
        self.app = app
        self.format = format

    def run(self):
        cli = InfluxDBClient("localhost", 8086, database="db")
        cli.create_database("db")
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
            client.connect(("localhost", 8080))
            while True:
                data = client.recv(1024)
                payload = {
                            "measurement": "light",
                            "time": None,
                            "tags": {},
                            "fields": {"value": None},
                        }
                payloads = []
                for measure in data.decode("utf-8").rstrip().split("\n"):
                    light = measure.strip("\n")
                    current_time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
                    self.app.info_lable["text"] = self.format.format(light)
                    payload["time"] = current_time
                    payload["fields"]["value"] = light
                    payloads.append(payload)
                cli.write_points(payloads, database="db")
                payloads = []


root = tk.Tk()


class History(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.master = master
        self.create_widgets()

    def create_widgets(self):
        self.log_panel = ScrolledText(self.master, wrap=tk.WORD, width=100, height=55,)
        self.log_panel.grid(padx=10, pady=10,row=1, sticky=tk.W)

    def update_log(self):
        cli = InfluxDBClient("localhost", 8086, database="db")
        data = cli.query("select value from light order by time desc limit 100;", database="db")
        self.log_panel.delete("1.0", tk.END)
        for res in data:
            log = "\n".join([f"[{point['time']}]: {point['value']} lux\n" for point in res])
            self.log_panel.insert(tk.END, log)
        self.log_panel.see("end")
        self.after(1000, self.update_log)


class App(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def open_chronograf(self):
        newWin = tk.Toplevel(self)
        self.log_pabel = History(newWin)
        self.log_pabel.after(1000, lambda: self.log_pabel.update_log())
        self.log_pabel.mainloop()

    def create_widgets(self):
        self.info_lable = tk.Label(self)
        self.info_lable["text"] = ""
        self.info_lable.pack(side="top")

        self.quit = tk.Button(self, text="Выйти", fg="red", command=self.master.destroy)
        self.browser = tk.Button(
            self, text="Открыть историю", command=self.open_chronograf
        )
        self.browser.pack(side="bottom")
        self.quit.pack(side="bottom")


app = App(master=root)
communicate_thread = CommunicateThread(app, "Cвет\n {} lx")
communicate_thread.start()
root.geometry("500x500")
app.mainloop()
